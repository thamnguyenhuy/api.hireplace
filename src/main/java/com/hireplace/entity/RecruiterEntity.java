package com.hireplace.entity;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.lang.String;
import java.util.List;

@Entity
@Cache
public class RecruiterEntity {
	@Id public Long id;

	@Index public String code;
	@Index public String name;
	public String description;
	public boolean active;

	public RecruiterEntity() {
		this.id = null;
	}
}

