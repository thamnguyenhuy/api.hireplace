package com.hireplace.api;
import com.google.api.server.spi.config.*;

import com.hireplace.business.RecruiterBusiness;
import com.hireplace.model.ErrorResponse;
import com.hireplace.model.request.RecruiterRequest;
import com.hireplace.model.request.RecruiterRequestEdit;

import com.hireplace.model.response.BaseResponse;
import com.hireplace.model.response.RecruiterResponse;
import com.hireplace.model.response.ItemResponse;


import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


@Api(
  name = "hireplace",
  version = "v1",
    apiKeyRequired = AnnotationBoolean.TRUE,
  namespace =
  @ApiNamespace(
    ownerDomain = "api.hireplace.com",
    ownerName = "api.hireplace.com"
  )
)

public class RecruiterAPI extends BaseAPI {
  private static final Logger logger = Logger.getLogger(RecruiterAPI.class.getName());


  @ApiMethod(httpMethod = ApiMethod.HttpMethod.PUT, name = "add", path = "recruiters")
  public BaseResponse add(RecruiterRequest request) {
    BaseResponse response = new BaseResponse();

    try {
      //validate base on the base api.
      List<ErrorResponse> validationResponse = this.validate(request);

      if (validationResponse.size() == 0) {

        RecruiterResponse addResult = RecruiterBusiness.getInstance().add(request);

        if (addResult != null){
          //set the payload
          response.setPayload(addResult);
        } else {
          response.setError(ErrorResponse.getAddFail());
        }
      } else {
        response.setErrors(validationResponse);
      }

    }catch (Exception exception){
      logger.log(Level.SEVERE, exception.getMessage());
    }
    return response;
  }

  @ApiMethod(httpMethod = ApiMethod.HttpMethod.POST, name = "edit", path = "recruiters")
  public BaseResponse edit(RecruiterRequestEdit request) {
    BaseResponse response = new BaseResponse();

    try {
      //validate base on the base api.
      List<ErrorResponse> validationResponse = this.validate(request);

      if (validationResponse.size() == 0) {

        RecruiterResponse editResult = RecruiterBusiness.getInstance().edit(request);

        if (editResult != null){
          //set the payload
          response.setPayload(editResult);
        } else {
          response.setError(ErrorResponse.getEditFail());
        }
      } else {
        response.setErrors(validationResponse);
      }

    }catch (Exception exception){
      logger.log(Level.SEVERE, exception.getMessage());
    }
    return response;
  }

  @ApiMethod(httpMethod = ApiMethod.HttpMethod.GET, name = "get", path = "recruiters/{id}")
  public BaseResponse get(@Named("id") Long id) {
    BaseResponse response = new BaseResponse();

    try {
      RecruiterResponse getResult = RecruiterBusiness.getInstance().get(id);
      if (getResult != null){
        //set the payload
        response.setPayload(getResult);
      } else {
        response.setError(ErrorResponse.getEditFail());
      }
    }catch (Exception exception){
      logger.log(Level.SEVERE, exception.getMessage());
    }
    return response;
  }

  @ApiMethod(httpMethod = ApiMethod.HttpMethod.GET, name = "gets", path = "recruiters")
  public BaseResponse gets() {
    BaseResponse response = new BaseResponse();

    try {
      List<RecruiterResponse> getResults = RecruiterBusiness.getInstance().gets();

      if (getResults != null){
        //set the payload
        List<ItemResponse> itemResponses = new ArrayList<ItemResponse>();
        for (RecruiterResponse result: getResults) {
          itemResponses.add(result);
        }
        response.setPayloads(itemResponses);
      } else {
        response.setError(ErrorResponse.noRecordFound());
      }

    }catch (Exception exception){
      logger.log(Level.SEVERE, exception.getMessage());
    }
    return response;
  }


  @ApiMethod(httpMethod = ApiMethod.HttpMethod.DELETE,  name = "delete", path = "recruiters/{id}")
  public BaseResponse delete(@Named("id") Long id) {
    BaseResponse response = new BaseResponse();

    try {
      boolean deleteResult = RecruiterBusiness.getInstance().delete(id);

      if (deleteResult == false){
        response.setError(ErrorResponse.getDeleteFail());
      }
    }catch (Exception exception){
      logger.log(Level.SEVERE, exception.getMessage());
    }
    return response;
  }
}
