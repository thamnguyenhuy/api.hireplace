package com.hireplace.api;

import com.hireplace.model.ErrorResponse;
import com.hireplace.validation.ModelValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by tom on 7/6/17.
 */
public class BaseAPI {
	private static final Logger LOG = Logger.getLogger(BaseAPI.class.getName());

	protected List<ErrorResponse> validate(Object object){
		List<ErrorResponse> errors = new ArrayList<>();
		try{
			List<String> validateMessages = ModelValidator.validate(object);

			//set errors
			for (String message : validateMessages){
				System.out.println(message);
				ErrorResponse error = new ErrorResponse();
				error.setMessage(message);
				errors.add(error);
			}
		} catch (Exception ex){
			LOG.log(Level.SEVERE, "validation in BaseAPI has error: " + ex.getMessage());
		}


		return errors;
	}

}
