package com.hireplace.business;

import com.hireplace.datastore.RecruiterDataStore;
import com.hireplace.entity.RecruiterEntity;
import com.hireplace.model.request.RecruiterRequest;
import com.hireplace.model.request.RecruiterRequestEdit;
import com.hireplace.model.response.RecruiterResponse;

import java.util.ArrayList;
import java.util.List;

public class RecruiterBusiness {
	private static RecruiterBusiness instance;

	private RecruiterBusiness(){}

	public static RecruiterBusiness getInstance(){
		if(instance == null){
			instance = new RecruiterBusiness();
		}
		return instance;
	}

	public RecruiterResponse add(RecruiterRequest request){
		RecruiterResponse response = new RecruiterResponse();
		try {
			RecruiterEntity entity = fromRequest(request);

			RecruiterDataStore.getInstance().add(entity);

			response = fromEntity(entity);
		}catch (Exception ex){
			throw ex;
		}
		return response;
	}

	public RecruiterResponse edit(RecruiterRequestEdit request){
		RecruiterResponse response = new RecruiterResponse();
		try {
			RecruiterEntity entity = fromRequestEdit(request);

			RecruiterDataStore.getInstance().edit(entity);

			response = fromEntity(entity);
		}catch (Exception ex){
			throw ex;
		}
		return response;
	}

	public boolean delete(Long key){
		boolean response;
		try {
			response = RecruiterDataStore.getInstance().delete(key);
		}catch (Exception ex){
			throw ex;
		}
		return response;
	}

	public RecruiterResponse get(Long key){
		RecruiterResponse response = new RecruiterResponse();
		try {

			RecruiterEntity entity = RecruiterDataStore.getInstance().get(key);
			if (entity != null) {
				response = fromEntity(entity);
			}
		}catch (Exception ex){
			throw ex;
		}
		return response;
	}

	public List<RecruiterResponse> gets(){
		List<RecruiterResponse> responses = new ArrayList<>();
		try {

			List<RecruiterEntity> entities = RecruiterDataStore.getInstance().gets();
			if (entities != null && entities.size() > 0) {
				for (RecruiterEntity entity: entities) {
					RecruiterResponse response = fromEntity(entity);
					responses.add(response);
				}
			}
		}catch (Exception ex){
			throw ex;
		}
		return responses;
	}


	private RecruiterEntity fromRequest(RecruiterRequest request){
		RecruiterEntity entity = new RecruiterEntity();
		entity.name = request.name;
		entity.code = request.code;

		entity.description = request.description;
		entity.active = request.active;

		return  entity;
	}


	private RecruiterEntity fromRequestEdit(RecruiterRequestEdit request){
		RecruiterEntity entity = fromRequest(request);
		entity.id = request.id;

		return  entity;
	}


	private RecruiterResponse fromEntity(RecruiterEntity entity){
		RecruiterResponse response = new RecruiterResponse();
		response.id = entity.id;
		response.code = entity.code;
		response.name = entity.name;
		response.description = entity.description;
		response.active = entity.active;

		return  response;
	}
}
