package com.hireplace.datastore;


	import com.googlecode.objectify.ObjectifyService;
	import com.hireplace.entity.RecruiterEntity;

	import javax.servlet.ServletContextListener;
	import javax.servlet.ServletContextEvent;

/**
 * OfyHelper, a ServletContextListener, is setup in web.xml to run before a JSP is run.  This is
 * required to let JSP's access Ofy.
 **/
public class OfyHelper implements ServletContextListener {
	public void contextInitialized(ServletContextEvent event) {
		ObjectifyService.register(RecruiterEntity.class);
	}

	public void contextDestroyed(ServletContextEvent event) {
		// App Engine does not currently invoke this method.
	}
}
