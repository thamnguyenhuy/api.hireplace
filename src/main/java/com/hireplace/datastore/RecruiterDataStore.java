package com.hireplace.datastore;

import com.hireplace.entity.RecruiterEntity;
import com.googlecode.objectify.ObjectifyService;
import static com.googlecode.objectify.ObjectifyService.ofy;
import java.util.List;

public class RecruiterDataStore {
	private static RecruiterDataStore instance;

	private RecruiterDataStore(){}

	public static RecruiterDataStore getInstance(){
		if(instance == null){
			instance = new RecruiterDataStore();

			ObjectifyService.register(RecruiterEntity.class);
		}
		return instance;
	}

	public RecruiterEntity add(RecruiterEntity entity){
		ofy().save().entity(entity).now();

		return entity;
	}

	public RecruiterEntity edit(RecruiterEntity entity){
		RecruiterEntity current = this.get((entity.id));
		if (current != null){
			current.code = entity.code;
			current.name = entity.name;
			current.description = entity.description;
			current.active = entity.active;

			ofy().save().entity(current).now();
		}
		return current;
	}

	public boolean delete(Long key){
		RecruiterEntity current = this.get(key);
		if (current != null){
			ofy().delete().entity(current).now();
			return true;
		}
		return false;
	}

	public RecruiterEntity get(Long key){
		RecruiterEntity entity =
		ofy().load().type(RecruiterEntity.class).id(key).now();

		return entity;
	}

	public List<RecruiterEntity> gets(){
		List<RecruiterEntity> results = ofy().load().type(RecruiterEntity.class).list();

		return results;
	}
}
