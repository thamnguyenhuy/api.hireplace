package com.hireplace.model;

/**
 * Created by tom on 30/5/17.
 */
public class StatusResponse {
    private int code;
    private String message;
    private String uid;
    private String version;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public StatusResponse(){
        this.code = 200; //success
        this.message = "OK";
        this.version = "0.1";
    }
}
