package com.hireplace.model.response;

import com.hireplace.model.Paging;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tom on 30/5/17.
 */
public class PayloadResponse {
    private List<ItemResponse> items;

    public PayloadResponse(){
        this.setItems(null);
    }

    public PayloadResponse(ItemResponse itemResponse){
        this.items = new ArrayList<>();
        this.items.add(itemResponse);
    }

    public PayloadResponse(List<ItemResponse> itemResponses){
        this.setItems(itemResponses);
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    public void setItems(List<ItemResponse> items) {
        this.items = items;
    }
}
