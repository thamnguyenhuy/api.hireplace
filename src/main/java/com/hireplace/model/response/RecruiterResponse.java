package com.hireplace.model.response;

import java.util.List;
/**
 * Created by tom on 30/5/17.
 */
public class RecruiterResponse extends ItemResponse{
    public Long id;
    public String code;
    public String name;
    public String description;
    public boolean active;
}
