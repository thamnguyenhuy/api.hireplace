package com.hireplace.model.response;

import com.hireplace.model.ErrorResponse;
import com.hireplace.model.Paging;
import com.hireplace.model.StatusResponse;

import java.util.*;

/**
 * Created by tom on 30/5/17.
 */
public class BaseResponse {
    private StatusResponse response;
    private Paging paging;
    private List<ErrorResponse> errors;
    private PayloadResponse payload;

    public BaseResponse() {
        this.response = new StatusResponse();
        this.paging = null;
        this.errors = null;
        this.payload = new PayloadResponse();
    }

    public StatusResponse getResponse() {
        return response;
    }

    public void setResponse(StatusResponse response) {
        this.response = response;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public List<ErrorResponse> getErrors() {
        return errors;
    }

    public void setError(ErrorResponse error) {
        this.errors = new ArrayList<>();
        this.errors.add(error);
    }

    public void setErrors(List<ErrorResponse> errors) {
        this.errors = errors;
    }

    public PayloadResponse getPayload() {
        return payload;
    }

    public void setPayload(ItemResponse itemResponse) {
        this.payload = new PayloadResponse(itemResponse);
    }

    public void setPayloads(List<ItemResponse> itemResponses) {
        this.payload = new PayloadResponse(itemResponses);
    }
}
