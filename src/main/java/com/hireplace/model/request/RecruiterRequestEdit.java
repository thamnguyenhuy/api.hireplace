package com.hireplace.model.request;

import com.hireplace.validation.Required;

/**
 * Created by tom on 15/6/17.
 */
public class RecruiterRequestEdit extends RecruiterRequest {
	@Required(fieldName = "{com.hireplace.field.recruiter.id.required}")
	public Long id;
}
