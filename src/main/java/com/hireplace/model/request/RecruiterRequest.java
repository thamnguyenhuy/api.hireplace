package com.hireplace.model.request;

import com.hireplace.validation.*;
import java.util.List;

/**
 * Created by tom on 29/5/17.
 */
public class RecruiterRequest extends BaseRequest {
	@Required (fieldName = "{com.hireplace.field.recruiter.code.required}")
	public String code;

	@Required (fieldName = "{com.hireplace.field.recruiter.name.required}")
	public String name;

	@Required (fieldName = "{com.hireplace.field.recruiter.description.required}")
	public String description;

	@Required (fieldName = "{com.hireplace.field.recruiter.active.required}")
	public boolean active;
}
