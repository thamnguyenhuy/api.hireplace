package com.hireplace.model.request;
import  com.hireplace.model.Paging;

/**
 * Created by tom on 29/5/17.
 */
public class BaseRequest {
    public Long displayOrder;
    public Paging paging = null;
}
