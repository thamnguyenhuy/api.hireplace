package com.hireplace.model;

/**
 * Created by tom on 30/5/17.
 */
    public class ErrorResponse {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ErrorResponse getAddFail(){
        ErrorResponse error = new ErrorResponse();
        error.setCode(400);
        error.setMessage("Fail to add the data.");
        return error;
    }

    public static ErrorResponse getEditFail(){
        ErrorResponse error = new ErrorResponse();
        error.setCode(400);
        error.setMessage("Fail to edit the data.");
        return error;
    }

    public static ErrorResponse noRecordFound(){
        ErrorResponse error = new ErrorResponse();
        error.setCode(400);
        error.setMessage("No data found.");
        return error;
    }

    public static ErrorResponse getDeleteFail(){
        ErrorResponse error = new ErrorResponse();
        error.setCode(400);
        error.setMessage("Fail to delete.");
        return error;
    }
}
