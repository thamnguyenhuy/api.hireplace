package com.hireplace.model;

/**
 * Created by tom on 29/5/17.
 */
public class Paging {
    private long totalPage = 0; //total records
    private long currentPage = 0; //current page
    private long itemPerPage = 0; //number or record per page

    public Paging() {}

    public long getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }

    public long getItemPerPage() {
        return itemPerPage;
    }

    public void setItemPerPage(long itemPerPage) {
        this.itemPerPage = itemPerPage;
    }
}
