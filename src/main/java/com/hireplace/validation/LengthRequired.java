package com.hireplace.validation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface LengthRequired {
	public long from() default Long.MIN_VALUE;
	public long to() default Long.MAX_VALUE;
	String message() default "{com.example.constraint.size.minMax}";
	String fieldName() default "";
}
