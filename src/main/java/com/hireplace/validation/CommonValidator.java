package com.hireplace.validation;


import java.lang.String;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.UrlValidator;


public class CommonValidator {

	public boolean isValidRequire(String field){
		return (field != null && !field.isEmpty());
	}

	public static boolean isValidEmail(String email){
		EmailValidator emailvalidator = EmailValidator.getInstance();
		return emailvalidator.isValid(email);
	}

	public boolean isValidPhone(String phone){
		return true;
	}

	public static boolean isValidUrl(String url){
		UrlValidator urlValidator = UrlValidator.getInstance();
		return urlValidator.isValid(url);
	}

	public boolean isValidDate(String date, String format){
		return true;
	}

	public boolean isValidDateTime(String dateTime, String format){
		return true;
	}

	// Min Max
	public boolean isValidMin(long number, long min){
		return number >= min;
	}

	public boolean isValidMax(long number, long max){
		return number <= max;
	}

	public boolean isValidMinMax(long number, long min, long max){
		return number >= min && number <= max;
	}

	public boolean isValidMin(double number, double min){
		return number >= min;
	}

	public boolean isValidMax(double number, double max){
		return number <= max;
	}

	public boolean isValidMinMax(double number, double min, double max){
		return number >= min && number <= max;
	}
	// End Min Max




}