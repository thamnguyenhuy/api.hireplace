package com.hireplace.validation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface EmailRequired {
	public boolean value() default true;
	String message() default "{com.example.constraint.required.email}";
	String fieldName() default "";
}

