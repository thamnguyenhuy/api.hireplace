package com.hireplace.validation;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ModelValidator {

	public static List<String> validate(Object objectToValidate){

		List<String> results = new ArrayList<String>();
		if (objectToValidate != null){
			Field[] declaredFields = objectToValidate.getClass().getDeclaredFields();

			for(Field field : declaredFields) {
				//validate required fields
				String required = validateRequired(field, objectToValidate);
				if (required.length() > 0){
					results.add(required);
				}

				//validate required email if the field is email
				String emailRequired = validateEmailRequired(field, objectToValidate);
				if (emailRequired.length() > 0){
					results.add(emailRequired);
				}

				//validate required email if the field is email
				String urlRequired = validateUrlRequired(field, objectToValidate);
				if (urlRequired.length() > 0){
					results.add(urlRequired);
				}

				//check for Size From/To
				String sizeRequired = validateLengthRequired(field, objectToValidate);
				if (sizeRequired.length() > 0){
					results.add(sizeRequired);
				}
				//check for Min
				//check for Max
				//check for Size To
				//Check for Email
				//check for Url
			}
		} else {
			results.add("The input object is null."); //the constant string need to be used.
		}

		return results;
	}

	private static String validateRequired(Field field, Object objectToValidate) {
		String message = "";
		Annotation annotationRequired = field.getAnnotation(Required.class);

		if (annotationRequired != null) {
			Required required = (Required) annotationRequired;

			if (required.value()) {
				field.setAccessible(true);
				try {
					if (field.get(objectToValidate) == null) {
						message = "{" + field.getName() + "}"+ required.fieldName() + required.message();
					}
				}catch (IllegalAccessException ex){
					message = field.getName() + "." + required.message();
				}
			}
		}

		return message;
	}

	private static String validateEmailRequired(Field field, Object objectToValidate) {
		String message = "";

		Annotation annotation = field.getAnnotation(EmailRequired.class);
		if (annotation != null) {
			EmailRequired annotationRequired = (EmailRequired) annotation;

			if (annotationRequired.value()) {

				field.setAccessible(true);

				try {
					if (field.get(objectToValidate) == null ||
							!CommonValidator.isValidEmail(field.get(objectToValidate).toString())) {
						message = field.getName() + annotationRequired.fieldName() + annotationRequired.message();
					}
				}catch (IllegalAccessException ex){
					message = field.getName() + "." + annotationRequired.message();
				}
			}
		}

		return message;
	}

	private static String validateUrlRequired(Field field, Object objectToValidate) {
		String message = "";

		Annotation annotation = field.getAnnotation(UrlRequired.class);
		if (annotation != null) {
			UrlRequired annotationRequired = (UrlRequired) annotation;

			if (annotationRequired.value()) {

				field.setAccessible(true);

				try {
					if (field.get(objectToValidate) == null ||
							!CommonValidator.isValidUrl(field.get(objectToValidate).toString())) {
						message = field.getName() + annotationRequired.fieldName() + annotationRequired.message();
					}
				}catch (IllegalAccessException ex){
					message = field.getName() + "." + annotationRequired.message();
				}
			}
		}

		return message;
	}

	private static String validateLengthRequired(Field field, Object objectToValidate) {
		String message = "";

		Annotation annotation = field.getAnnotation(LengthRequired.class);
		if (annotation != null) {
			LengthRequired annotationRequired = (LengthRequired) annotation;

			if (annotationRequired.from() >= Long.MIN_VALUE || annotationRequired.to() <= Long.MAX_VALUE) {

				field.setAccessible(true);

				try {
					if (field.get(objectToValidate) == null ||
							field.get(objectToValidate).toString().length() < annotationRequired.from() ||
							field.get(objectToValidate).toString().length() > annotationRequired.to()) {
						message = field.getName() + annotationRequired.fieldName() + annotationRequired.message();
					}
				}catch (IllegalAccessException ex){
					message = field.getName() + "." + annotationRequired.message();
				}
			}
		}

		return message;
	}
}