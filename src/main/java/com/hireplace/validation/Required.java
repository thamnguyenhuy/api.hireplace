package com.hireplace.validation;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Required {
	public boolean value() default true;
	String message() default "{com.hireplace.field.required.message}";
	String fieldName() default "";
}
